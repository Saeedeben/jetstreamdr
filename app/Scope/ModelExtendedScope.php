<?php

namespace App\Scope;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ModelExtendedScope
 *
 * @package App\Scopes\User
 */
class ModelExtendedScope implements Scope
{
    /**
     * @var
     */
    private $value;
    /**
     * @var string
     */
    private $column;

    /**
     * AdminScope constructor.
     *
     * @param string $value
     * @param string $column
     */
    public function
    __construct(string $value, string $column)
    {
        $this->value  = $value;
        $this->column = $column;
    }


    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param \Illuminate\Database\Eloquent\Model   $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->where($this->column, $this->value);
    }
}
