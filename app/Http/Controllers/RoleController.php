<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        $guards = User::KINDS;

        return view('user.role', ['roles' => $roles, 'guards' => $guards]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return void
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Role::create(['name' => $request->name, 'guard_name' => $request->guard_name]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function permissionIndex(Request $request)
    {
        $permissions = Permission::with('roles')
            ->get()
            ->groupBy('guard_name')
            ->toArray();

        $guards = User::KINDS;

        return view('user.permission', ['permissions' => $permissions, 'guards' => $guards]);
    }

    public function addPermission(Request $request)
    {
        Permission::create(['name' => $request->name, 'guard_name' => $request->guard_name]);

        return redirect()->back();
    }

    public function assignIndex()
    {
        return view('user.makePermission');
    }

    public function assignPermission(Request $request)
    {
        foreach ($request->permissions as $guard => $permission) {
            if ($guard == 'admin')
                continue;
            $role = Role::where('guard_name', $guard)->first();

            $users = User::with('roles')
                ->where('kind', $guard)
                ->get();


            if ($role)
                foreach ($users as $user) {
                    $user->syncRoles($role->guard_name);
                }

            $role->permissions()->sync($permission);
        }

        return redirect()->back();
    }
}
