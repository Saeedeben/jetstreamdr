<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pages = Page::with('category');

        if ($request->category) {
            $pages->where('category_id', $request->category);
        }
        $pages = $pages->paginate();

        $categories = Category::query()->pluck('name', 'id');

        return view('page.index', ['pages' => $pages, 'categories' => $categories]);
    }

    public function create()
    {
        $categories = Category::query()->pluck('name', 'id');
        return view('page.new-page', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|array
     */
    public function store(Request $request)
    {
        $attributes = $request->all();

        \DB::beginTransaction();

        try {
            $page = new Page();
            $page->fill($attributes);
            $page->category()->associate($request->category);
            $page->save();

            \DB::commit();

            return redirect('/panel/page');

        } catch (\Exception $ex) {
            \DB::rollback();

            return [
                'success' => false,
                'message' => 'اشتباهی رخ داده است.',
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Page $page
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Page         $page
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Page $page
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Page $page)
    {
        $page->delete();

        return redirect()->back();
    }

    public function PageJson(Request $request)
    {
        $pages = Page::select('name', 'category_id');

        $pages = $pages->get()->groupBy('category_id');

        $response = [];
        foreach ($pages as $key => $page) {
            foreach ($page as $item) {
                $cat              = Category::findOrfail($key);
                $response[$cat->name][] = $item->name;
            }
        }

        return $response;
    }

}
