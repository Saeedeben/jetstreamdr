<?php

namespace App\Http\Controllers;

use App\Models\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $account = Account::query();

        if ($request->status) {
            $account->where('status', $request->status);
        }

        $account = $account->paginate();

        $statuses = Account::STATUSES;

        return view('account.index', ['accounts' => $account, 'statuses' => $statuses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|array
     */
    public function store(Request $request)
    {
        $attributes = $request->all();

        \DB::beginTransaction();
        try {
            $account         = new Account();
            $account->status = Account::STATUS_ACTIVE;
            $account->fill($attributes);
            $account->save();

            \DB::commit();
            return redirect(route('account.index'));

        } catch (\Exception $ex) {
            \DB::rollBack();

            \Log::info($ex);

            return [
                'success' => false,
                'message' => 'اشتباهی رخ داده است',
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Account $account
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Account $account
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Account      $account
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Account $account)
    {
        $status = Account::STATUSES;
        foreach ($status as $non) {
            if ($non != $request->status) {
                $change = $non;
            }
        }

        $account->update(['status' => $change]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Account $account
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Account $account)
    {
        $account->delete();

        return redirect()->back();
    }

    public function getJson()
    {
        /** @var Account $account */
        $account = Account::all();

        return $account;
    }

    public function updateBYBOT(Request $request, Account $account_page)
    {
        $attributes = $request->all();

        \DB::beginTransaction();
        try {
            $account_page->fill($attributes);
            $account_page->save();

            \DB::commit();

            return [
                'success' => true,
                'message' => 'به روزرسانی موفق',
            ];
        } catch (\Exception $ex) {
            \DB::rollBack();

            \Log::info($ex);

            return [
                'success' => false,
                'message' => 'خطایی رخ داد',
            ];
        }
    }

    public function deleteBYBOT(Request $request, Account $account_page)
    {
        $account_page->delete();

        return [
            'success' => true,
            'message' => 'پاک شد',
        ];
    }
}
