<?php

namespace App\Http\Controllers;

use App\Models\Data;
use App\Models\Page;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ImportDataController extends Controller
{

    public function getJson(Request $request)
    {
        $data = $request->json_of_page;

        if (!$request->json_of_page) {
            return [
                'success' => false,
                'message' => 'the request is empty',
            ];
        }
        $page = Page::select('id', 'name')
            ->where('name', $data[0]['username'])
            ->first();


        foreach ($data as $datum) {
            $post = new Data();

//            $time = Carbon::createFromTimestamp($datum['Time_Stamp'])->timezone('Asia/tehran');

            $post->views     = $datum['view'];
            $post->video_url = $datum['video_url'];
            $post->caption   = $datum['caption'];
            $post->post_url  = $datum['post_url'];
            $post->img_url   = $datum['img_url'];
            $post->hashtags  = $datum['hashtags'];
            $post->likes     = $datum['like'];
            $post->mentions  = $datum['mentions'];
            $post->comments  = $datum['comment'];
            $post->type      = $datum['type'];
            $post->page()->associate($page['id']);
            $post->save();

        }

        \Log::info($data);

        return [
            'success' => true,
            'message' => 'insert data in database successfully',
            'page'    => $page->name,
        ];
    }


    public function profilePic(Request $request)
    {
        $data = $request->details;

        $userName    = $data['username'];
        $profileUrl  = $data['profile_img_url'];
        $followers   = $data['followers'];
        $followings  = $data['followings'];
        $post_number = $data['post_numbers'];
        $fullName    = $data['full_name'];

        $page = Page::where('name', $userName);

        if ($page)
            $page->update([
                'logo_url'    => $profileUrl,
                'followers'   => $followers,
                'followings'  => $followings,
                'post_number' => $post_number,
                'full_name'   => $fullName,
            ]);

        return [
            'success' => true,
            'message' => 'profile details update successfully',
        ];
    }

}
