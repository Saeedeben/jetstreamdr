<?php

namespace App\Http\Controllers;

use App\Models\Data;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $time  = today()->subDays(5)->timezone('Asia/tehran')->format('Y-m-d H:i');
        $pages = Page::where('category_id', $request->category);

        $pageInfo = $pages->get()->pluck('name');

        if ($request->pageInsta) {
            $pages = $pages->where('name', $request->pageInsta);
        }
        $selectedPage = $pages->get()->pluck('id')
            ->toArray();

        $data = Data::with('page')
            ->whereIn('page_id', $selectedPage)
            ->where('created_at', '>=', $time);
        if ($request->filter) {
            $data = $data->orderBy("$request->filter", 'DESC');
        } else {
            $data = $data->orderBy('created_at', 'DESC');
        }

        $data = $data->get()
            ->toArray();

        $output = [];
        for ($i = 0; $i < count($data); $i++) {
            $pageFilter = [];
            foreach ($data as $key => $datum) {
                if (!in_array($datum['page']['name'], $pageFilter)) {
                    $output[]     = $datum;
                    $pageFilter[] = $datum['page']['name'];
                    unset($data[$key]);
                }
            }
        }

        $output = $this->paginate($output, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ], 9, $request->page);


        return view('instagram.index', ['output' => $output, 'pages' => $pageInfo, 'category' => $request->category, 'value' => $request->value]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Data $data
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Data $data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Data $data
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Data $data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Data         $data
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Data $data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Data $data
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Data $data)
    {
        //
    }

    public function telegram(Request $request)
    {
        $url = "https://api.telegram.org/bot1281624613:AAEdkttQzz3ORf04VKD_qkJbetbwwHJ92e0/sendVideo";

        $response = Http::post($url, [
            'video'   => $request->video,
            'caption' => $request->caption,
            'chat_id' => $request->chat_id,
        ]);

        if ($response['ok'] == 1)
            return \redirect()->back();
    }

    /**
     * The attributes that are mass assignable.
     *
     *
     * @param       $items
     * @param int   $perPage
     * @param null  $page
     * @param array $options
     *
     * @return LengthAwarePaginator
     */
    public function paginate($items, $options = [], $perPage = 9, $page = NULL)
    {
        $page  = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
