<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\User;
use Livewire\Component;

class App extends Component
{
    public function render()
    {
        $categories = Category::all()->pluck('name', 'id');

        $kinds = User::KINDS;

        return view('livewire.app', ['categories' => $categories, 'kinds' => $kinds]);
    }
}
