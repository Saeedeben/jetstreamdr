<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserPage
 *
 * @package App\Models\Instagram
 *
 * @property int    $id
 *
 * @property string $username
 * @property string $password
 *
 * @property string $status
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 */
class Account extends Model
{
    use HasFactory;

    protected $table = 'accounts';

    protected $fillable = [
        'username',
        'password',
        'status',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_BLOCK  = 'block';
    const STATUSES      = [
        self::STATUS_ACTIVE,
        self::STATUS_BLOCK,
    ];

    // ------------------------------------ Relations ------------------------------------
    // ------------------------------------ Attributes -----------------------------------
    // ------------------------------------ Methods --------------------------------------
}
