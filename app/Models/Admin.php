<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Admin
 *
 * @package App\Models
 *
 */
class Admin extends User
{
    use HasFactory;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->kind = self::KIND_ADMIN;

        $this->guard_name = self::KIND_ADMIN;
    }

    protected $guard_name = 'admin';

    // ------------------------------------ Relations ------------------------------------
    // ------------------------------------ Attributes ------------------------------------
    // ------------------------------------ Methods ------------------------------------

}
