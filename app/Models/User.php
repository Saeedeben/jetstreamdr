<?php

namespace App\Models;

use App\Scope\ModelExtendedScope;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 *
 * @package App\Models
 * @property int          $id
 *
 * @property string       $name
 * @property string       $email
 * @property-read  string $password
 *
 * @property string       $kind
 *
 * @property Carbon       $created_at
 * @property Carbon       $updated_at
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    const KIND_ADMIN  = 'admin';
    const KIND_MEMBER = 'member';
    const KINDS       = [
        self::KIND_ADMIN,
        self::KIND_MEMBER,
    ];

    /**
     * map user kind to class namespace
     */
    const KIND_OF_NAMESPACES = [
        'App\Models\Admin'  => self::KIND_ADMIN,
        'App\Models\Member' => self::KIND_MEMBER,
    ];

    const MAIN_USER_ID_IN_SESSION = 'MAIN_USER_ID_IN_SESSION';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $guard_name = self::KIND_ADMIN;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        $kind = self::KIND_OF_NAMESPACES[get_called_class()] ?? NULL;
        if ($kind)
            static::addGlobalScope(new ModelExtendedScope($kind, 'kind'));

    }

    public function guard(array $guarded)
    {
        $this->guarded = $guarded;

        return $this;
    }

    // ------------------------------------ Relations ------------------------------------
    // ------------------------------------ Attributes ------------------------------------
    // ------------------------------------ Methods ------------------------------------


}
