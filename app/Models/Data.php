<?php

namespace App\Models;

use App\Models\Page;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TooReal
 *
 * @package App\Models\Instagram
 *
 * @property int    $id
 *
 * @property int    $page_id
 * @property string $post_url
 * @property string $img_url
 * @property string $video_url
 * @property string $type
 * @property int    $likes
 * @property int    $comments
 * @property int    $views
 * @property int    $tagged
 * @property string $caption
 * @property array  $mentions
 * @property array  $hashtags
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * // ------------------------------------ Relations ------------------------------------
 * @property Page   $page
 */
class Data extends Model
{
    use HasFactory;

    protected $table = 'data';

    protected $fillable = [
        'page_id',
        'post_url',
        'img_url',
        'video_url',
        'likes',
        'comments',
        'views',
        'caption',
        'mentions',
        'hashtags',
    ];

    protected $casts = [
        'hashtags' => 'json',
        'mentions' => 'json',
    ];

    const TYPE_IMAGE = 'GraphImage';
    const TYPE_VIDEO = 'GraphVideo';
    const TYPE_ALBUM = 'GraphSidecar';
    const TYPES      = [
        self::TYPE_IMAGE,
        self::TYPE_VIDEO,
        self::TYPE_ALBUM,
    ];

    // ------------------------------------ Relations ------------------------------------
    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id');
    }
    // ------------------------------------ Attributes ------------------------------------
    // ------------------------------------ Methods ------------------------------------

}
