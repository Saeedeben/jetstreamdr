<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Member
 *
 * @package App\Models
 */
class Member extends User
{
    use HasFactory;


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->kind = self::KIND_MEMBER;
    }
    

    // ------------------------------------ Relations ------------------------------------
    // ------------------------------------ Attributes ------------------------------------
    // ------------------------------------ Methods ------------------------------------

}
