<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Page
 *
 * @package App\Models\Pages
 *
 * @property int      $id
 *
 * @property string   $name
 * @property int      $category_id
 * @property string   $full_name
 *
 * @property int      $post_number
 * @property int      $followers
 * @property int      $followings
 *
 * @property string   $logo_url
 *
 * @property Carbon   $created_at
 * @property Carbon   $updated_at
 *
 * // ------------------------------------ Relations ------------------------------------
 * @property Category $category
 */
class Page extends Model
{
    use HasFactory;

    protected $table = 'pages';

    protected $fillable = [
        'name',
        'category_id',
        'post_number',
        'followers',
        'followings',
        'full_name',
    ];


    // ------------------------------------ Relations ------------------------------------
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    // ------------------------------------ Attributes -----------------------------------
    // ------------------------------------ Methods --------------------------------------

}
