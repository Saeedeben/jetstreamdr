<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;

class PermissionSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $permissionsCount = 0;
        $permissionClass  = config('permission.models.permission');
        $routeKinds       = include "config/user_permission.php";
        $permissionsCount += count($routeKinds, COUNT_RECURSIVE);

        $bar = $this->output->createProgressBar($permissionsCount);
        foreach (User::KINDS as $kind) {
            if ($kind == User::KIND_MEMBER)
                continue;
            $permissionEntity = call_user_func([$permissionClass, 'query']);
            $permissions      = [];
            foreach ($routeKinds['panel'] as $name) {
                $permissions[$name] = [
                    'name'       => $name,
                    'guard_name' => $kind,
                ];
            }
            $oldPermissions = $permissionEntity
                ->where('guard_name', $kind)
                ->get()
                ->keyBy('name')
                ->toArray();

            $diffPermissions = array_values(array_diff_key($permissions, $oldPermissions));

            $permissionEntity->insert($diffPermissions);
            $bar->advance();
        }

        $bar->finish();

    }
}
