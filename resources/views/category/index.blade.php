<style>
    .pg p {
        display: none;
    }
</style>
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

                <form class="text-center border border-light p-5 w-full sm:w-1/2 mx-auto"
                      action="{{route('category.store')}}" method="post">
                    @csrf

                    <p class="h4 mb-4">Create New Category</p>

                    <!-- Name -->
                    <input type="text" id="defaultContactFormName" class="form-control mb-4" placeholder="Name"
                           name="name">

                    <!-- password -->
                    <input type="text" id="defaultContactFormName" class="form-control mb-4" placeholder="timeToWork"
                           name="time_to_work">

                    <!-- Send button -->
                    <button class="btn btn-info btn-block" type="submit">Create</button>

                </form>

                <div class="w-full mx-auto">
                    <div class="bg-white shadow-md rounded my-6 mx-2">
                        <table class="text-left w-full border-collapse">
                            <!--Border collapse doesn't work on this site yet but it's available in newer tailwind versions -->
                            <thead>
                            <tr>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    id
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Name
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    time to work
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Action
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr class="hover:bg-grey-lighter">
                                    <td class="py-2 px-2 border-b border-grey-light">{{$category->id}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{$category->name}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{$category->time_to_work}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">

                                        <form class="my-2" action="/panel/category/{{$category->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button
                                                class="text-gray-lighter font-bold py-1 px-3 rounded text-xs bg-red-100 hover:bg-blue-dark">
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="pg mx-auto w-1/3 m-3 justify-center">
                    {{$categories->render()}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
