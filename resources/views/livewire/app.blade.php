<div class="h-full">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>Sidebar Menu</h3>
        </div>

        <ul class="list-unstyled components">
            <p class="text-center">دانالیزور</p>
            @can('pages')
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">Pages</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="{{route('page.index')}}">Pages Index</a>
                        </li>
                        <li>
                            <a href="{{route('newPage')}}">New Page</a>
                        </li>
                        <li>
                            <a href="{{route('category.index')}}">Category</a>
                        </li>
                    </ul>
                </li>
            @endcan
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">Instagram</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    @foreach($categories as $key => $category)
                        <li>
                            <a href="/panel/data?category={{$key}}&value={{$category}}">{{$category}}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
            @can('users')

                <li class="active">
                    <a href="#userSubmenu" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">Users</a>
                    <ul class="collapse list-unstyled" id="userSubmenu">
                        @foreach($kinds as $kind)
                            <li>
                                <a href="/panel/user/{{$kind}}">{{$kind}}</a>
                            </li>
                        @endforeach
                        <li>
                            <a href="/panel/user/permissions">Permissions</a>
                        </li>
                        <li>
                            <a href="/panel/user/roles">Roles</a>
                        </li>
                    </ul>
            @endcan
            @can('account')
                <li>
                    <a href="{{route('account.index')}}">Accounts</a>
                </li>
            @endcan
            <li class="active">
                <a href="#settingSubmenu" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">Setting</a>
                <ul class="collapse list-unstyled" id="settingSubmenu">
                    @foreach($categories as $key => $category)
                        <li>
                            <a href="#">{{$category}}</a>
                        </li>
                    @endforeach
                </ul>

            <li>
                <a href="#">Contact</a>
            </li>
        </ul>
    </nav>
</div>
