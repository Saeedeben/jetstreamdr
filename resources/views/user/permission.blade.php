@can('users')
<style>
    .pg p {
        display: none;
    }
</style>
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <form action="/panel/user/assign" method="post">
                    @csrf
                    @foreach($permissions as $key =>$permission)
                        <div class=" m-4">
                            <h3 class="text-gray-700">{{$key}}</h3>
                            <div class="mt-2">
                                @foreach($permission as $per)
                                    <div class="inline-block mr-5">
                                        <label class=" items-center mt-3">
                                            <input type="checkbox" name="permissions[{{$key}}][{{$per['id']}}]"
                                                   class="form-checkbox h-5 w-5 text-red-600"
                                                   {{ $per['roles'] ? $per['roles'][0]['guard_name'] == $key ?  'checked=checked' :'':'' }} value="{{$per['id']}}"><span
                                                class="ml-2 text-gray-700">{{$per['name']}}</span>
                                        </label>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                    <button class="btn btn-danger m-4" type="submit">Assign Or Update</button>

                </form>

            </div>
        </div>
    </div>
</x-app-layout>
@endcan
