@can('users')
<style>
    .pg p {
        display: none;
    }
</style>
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="m-3 p-3">
                    <form class="border-2 p-2 w-1/2 mx-auto" action="/panel/user/roles" method="post">
                        @csrf
                        <h3 class="mx-auto text-red-400">Create New Role</h3>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name of Role</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp" placeholder="Name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Role Guard Name</label>
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1" name="guard_name">
                                    @foreach($guards as $guard)
                                        <option value="{{$guard}}">{{$guard}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                    <table class="table table-striped table-dark">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Guard Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$role->id}}</td>
                                <td>{{$role->name}}</td>
                                <td>{{$role->guard_name}}</td>
                                <td>action</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
@endcan
