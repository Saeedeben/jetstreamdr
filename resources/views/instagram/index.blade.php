<style>
    .pg p {
        display: none;
    }
</style>
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <form class="form-inline m-3" action="/panel/data">
                    <input type="text" value="{{$category}}" name="category" hidden>
                    <input type="text" value="{{$value}}" name="value" hidden>
                    <select class="browser-default custom-select px-3" name="filter">
                        <option value="likes">Likes</option>
                        <option value="views">Views</option>
                        <option value="comments">Comments</option>
                    </select>
                    <button type="submit" class="btn btn-primary mx-4">Filter</button>
                </form>
                <!-- component -->
                <div class="container my-12 mx-auto px-4 md:px-12">
                    <h1 class="mx-auto">{{$value}}</h1>
                    <div class="flex flex-wrap -mx-1 lg:-mx-4">

                    @foreach($output as $datum)
                        <!-- Column -->
                            <div class="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">

                                <!-- Article -->
                                <article class="overflow-hidden rounded-lg shadow-lg">

                                    {{--                                    <a href="#">--}}
                                    {{--                                        <img alt="Placeholder" class="block h-64 w-full"--}}
                                    {{--                                             src="{{$datum['img_url']}}">--}}
                                    {{--                                    </a>--}}
                                    @if($datum['video_url'])
                                        <video controls style="height: 300px; width: 100%; display: block;"
                                               poster="{{$datum['img_url']}}">
                                            <source src={{$datum['video_url']}} type=video/ogg>
                                            <source src={{$datum['video_url']}} type=video/mp4>
                                        </video>
                                    @else
                                        <img class="card-img-top"
                                             data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                             alt="{{$datum['hashtags'] ? implode(" ",$datum['hashtags']) : 'دانالیزور'}}"
                                             style="height: 300px; width: 100%; display: block;object-fit: scale-down;"
                                             src="{{$datum['img_url']}}"
                                             data-holder-rendered="true">
                                    @endif

                                    <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                                        <form
                                            action="{{route('telegram')}}"
                                            method="post">
                                            @csrf
                                            <input type="text" name="chat_id" value="@fucktooreal" hidden>
                                            <input type="text" name="caption" value="{{$datum['caption']}}" hidden>
                                            <input type="text" name="video"
                                                   value="{{$datum['video_url'] ? $datum['video_url'] : $datum['img_url']}}"
                                                   hidden>
                                            <button type="submit">
                                                <i class="fa fa-share-alt" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                        <h1 class="text-lg">
                                            <a class="no-underline hover:underline text-black" href="#">
                                                {{$datum['page']['name']}}
                                            </a>
                                        </h1>

                                        <p class="text-grey-darker text-sm">
                                            {{$datum['likes']}}
                                            <i class="fa fa-heart"></i>
                                        </p>

                                    </header>

                                    <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                                        <a class="flex items-center no-underline hover:underline text-black" href="#">
                                            <p class="ml-2 text-sm text-black overflow-auto h-20">
                                                {{$datum['caption']}}
                                            </p>
                                        </a>
                                        <a class="no-underline text-grey-darker hover:text-red-dark" href="#">
                                            <span class="hidden">Like</span>
                                        </a>
                                    </footer>

                                </article>
                                <!-- END Article -->

                            </div>
                            <!-- END Column -->
                        @endforeach
                    </div>
                </div>


                <div class="pg mx-auto w-1/3 m-3 justify-center">
                    {{$output->appends(request()->query())->links()}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
