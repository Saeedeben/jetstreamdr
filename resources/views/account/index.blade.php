@can('account')
<style>
    .pg p {
        display: none;
    }
</style>
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <form class="text-center border border-light p-5 w-full sm:w-1/2 mx-auto"
                      action="{{route('account.store')}}" method="post">
                    @csrf

                    <p class="h4 mb-4">Create New Account</p>

                    <!-- Name -->
                    <input type="text" id="defaultContactFormName" class="form-control mb-4" placeholder="username"
                           name="username">

                    <!-- password -->
                    <input type="text" id="defaultContactFormName" class="form-control mb-4" placeholder="Password"
                           name="password">

                    <!-- Send button -->
                    <button class="btn btn-info btn-block" type="submit">Create</button>

                </form>

                <form class="form-inline m-3" action="{{route('account.index')}}">
                    <select class=" w-1/3 border-2 rounded px-3" name="status">
                        @foreach($statuses as $status)
                            <option value="{{$status}}">{{$status}}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-primary mx-4">Filter</button>
                </form>
                <div class="w-full mx-auto">
                    <div class="bg-white shadow-md rounded my-6 mx-2">
                        <table class="text-left w-full border-collapse">
                            <!--Border collapse doesn't work on this site yet but it's available in newer tailwind versions -->
                            <thead>
                            <tr>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    id
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    UserName
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Password
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Status
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($accounts as $account)
                                <tr class="hover:bg-grey-lighter">
                                    <td class="py-2 px-2 border-b border-grey-light">{{$account->id}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{$account->username}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{$account->password}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{$account->status}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">

                                        <form class="my-2" action="/panel/account/{{$account->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button
                                                class="text-gray-lighter font-bold py-1 px-3 rounded text-xs bg-red-100 hover:bg-blue-dark">
                                                Delete
                                            </button>
                                        </form>
                                        <form class="my-2" action="/panel/account/{{$account->id}}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <input type="text" name="status" value="{{$account->status}}" hidden>
                                            <button
                                                class="text-gray-lighter font-bold py-1 px-3 rounded text-xs bg-blue-100 hover:bg-blue-dark">
                                                CHMOD
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="pg mx-auto w-1/3 m-3 justify-center">
                    {{$accounts->render()}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
@endcan
