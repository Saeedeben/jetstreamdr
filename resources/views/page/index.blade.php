@can('pages')
<style>
    .pg p {
        display: none;
    }
</style>
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <form class="form-inline m-3" action="{{route('page.index')}}">
                    <select class="browser-default custom-select px-3" name="category">
                        @foreach($categories as $key =>$category)
                            <option value="{{$key}}">{{$category}}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-primary mx-4">Filter</button>
                </form>
                <div class="w-full mx-auto">
                    <div class="bg-white shadow-md rounded my-6 mx-2">
                        <table class="text-left w-full border-collapse">
                            <!--Border collapse doesn't work on this site yet but it's available in newer tailwind versions -->
                            <thead>
                            <tr>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    id
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Name
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Category
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    post_num
                                </th>
                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    followers
                                </th>

                                <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pages as $page)
                                <tr class="hover:bg-grey-lighter">
                                    <td class="py-2 px-2 border-b border-grey-light">{{$page->id}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{$page->name}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{$page->category->name}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{$page->post_number}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{$page->followers}}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">

                                        <form class="my-2" action="/panel/page/{{$page->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button
                                                class="text-gray-lighter font-bold py-1 px-3 rounded text-xs bg-red-100 hover:bg-blue-dark">
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="pg mx-auto w-1/3 m-3 justify-center">
                    {{$pages->render()}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
@endcan
