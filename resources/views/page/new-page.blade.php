@can('pages')
    <x-app-layout>
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <form class="text-center border border-light p-5 w-full sm:w-1/2 mx-auto"
                          action="{{route('page.store')}}" method="post">
                        @csrf

                        <p class="h4 mb-4">Create New Page</p>

                        <!-- Name -->
                        <input type="text" id="defaultContactFormName" class="form-control mb-4" placeholder="Name"
                               name="name">

                        <!-- Subject -->
                        <label>Category</label>
                        <select class="browser-default custom-select mb-4" name="category">
                            @foreach($categories as $key =>$category)
                                <option value="{{$key}}">{{$category}}</option>
                            @endforeach
                        </select>

                        <!-- Send button -->
                        <button class="btn btn-info btn-block" type="submit">Create</button>

                    </form>
                </div>
            </div>
        </div>
    </x-app-layout>
@endcan
