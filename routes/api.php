<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// ------------------------------------ bot API ------------------------------------
Route::post('/upload_json', [\App\Http\Controllers\ImportDataController::class, 'getJson']);
Route::post('/upload_json/page_profile_pic', [\App\Http\Controllers\ImportDataController::class, 'profilePic']);
Route::post('/upload_json_too_real', [\App\Http\Controllers\ImportDataController::class, 'getTooReal']);
Route::get('/page_json', [\App\Http\Controllers\PageController::class, 'PageJson']);

Route::get('account_json', [\App\Http\Controllers\AccountController::class, 'getJson']);
Route::put('account_page/{account_page}/update_account', [\App\Http\Controllers\AccountController::class, 'updateBYBOT']);
Route::delete('account_page/{account_page}/delete_account', [\App\Http\Controllers\AccountController::class, 'deleteBYBOT']);
