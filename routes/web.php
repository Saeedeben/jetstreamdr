<?php

use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
    return view('dashboard');
});

Route::group(['prefix' => 'panel', 'middleware' => ['auth:sanctum', 'verified']], function () {
    Route::get('/new_page', [\App\Http\Controllers\PageController::class, 'create'])->name('newPage');
    Route::apiResource('/page', \App\Http\Controllers\PageController::class);

    Route::resource('/account', \App\Http\Controllers\AccountController::class);
    Route::resource('/category', \App\Http\Controllers\CategoryController::class);

    Route::resource('/data', \App\Http\Controllers\DataController::class);
    Route::post('/telegram', [\App\Http\Controllers\DataController::class, 'telegram'])->name('telegram');

    Route::group(['prefix' => 'user'], function () {
        Route::resource('admin', \App\Http\Controllers\AdminController::class);
        Route::resource('member', \App\Http\Controllers\MemberController::class);

        Route::resource('/roles', \App\Http\Controllers\RoleController::class);

        Route::get('/permissions', [\App\Http\Controllers\RoleController::class, 'permissionIndex']);
        Route::post('/permissions', [\App\Http\Controllers\RoleController::class, 'addPermission']);

        Route::post('/assign', [\App\Http\Controllers\RoleController::class, 'assignPermission']);
    });

});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

