<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('page_id')->unsigned();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');

            $table->string('post_url')->index();
            $table->text('img_url')->nullable();
            $table->text('video_url')->nullable();
            $table->enum('type', \App\Models\Data::TYPES)->nullable();


            $table->integer('likes')->nullable();
            $table->integer('comments')->nullable();
            $table->integer('views')->nullable();
            $table->integer('tagged')->nullable();

            $table->text('caption')->nullable();
            $table->json('mentions')->nullable();
            $table->json('hashtags')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}
